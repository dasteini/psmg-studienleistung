﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {
	// Stores the InputManager
	PlayerInputManager mInputManager;
	// Respawn position
	public Transform spawnPosition;

	// Event system to inform other objects about players health status
	public delegate void PlayerEvent(int currentPoints);
	public static event PlayerEvent OnHealthChanged;

	// Standard health points 
	private int defaultHealthPoints = 100;
	// Current HealthPoints
	public int mHealthPoint;

	public int healthPoint {
		get {
			return mHealthPoint;
		}

		set { 
			mHealthPoint = value;
		}
	}

	// Function to clearify the decrease of health points (more readable than simple set/get)
	public void decreaseHealthPoints(int value) {
		mHealthPoint -= value;
		OnHealthChanged (mHealthPoint);
	}

	public void SetDefaultHealthPoints() {
		mHealthPoint = defaultHealthPoints;
		OnHealthChanged (mHealthPoint);
	}

	// Use this for initialization
	void Start () {
		Init ();
	}

	void Init() {
		mInputManager = gameObject.GetComponent<PlayerInputManager> ();
		mInputManager.Init ();
		mInputManager.setPosition (spawnPosition);

		// Notify all components about the initial health points
		OnHealthChanged(mHealthPoint);
	}

	// Set player to respawn position
	public void respawnPlayer() {
		mInputManager.setPosition (spawnPosition);
	}
}
