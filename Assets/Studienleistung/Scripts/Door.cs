﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {
	public bool isOpen = false;
	// Values which specify min and max height of the door
	public float unlockDepth = 0.0f;
	public float lockDepth = 3.0f;
	public float lockSpeed = 3f;
	
	// Update is called once per frame
	void FixedUpdate () {
		// Animate and open/close the door depending on the isOpen value
		if (isOpen) {
			unlockDoor ();
		} else {
			lockDoor ();
		}
	}

	// Move door upward
	void lockDoor() {
		if (transform.position.y <= lockDepth) {
			transform.Translate (Vector3.up * lockSpeed * Time.deltaTime);
		}
	}

	// Move door downward
	void unlockDoor() {
		if (transform.position.y >= unlockDepth) {
			transform.Translate (Vector3.down * lockSpeed * Time.deltaTime);
		}
	}


}
