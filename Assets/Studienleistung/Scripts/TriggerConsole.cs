﻿using UnityEngine;
using System.Collections;

public class TriggerConsole : MonoBehaviour {
	private AudioSource audio;
	public GameObject triggerDoor;
	private Door door;

	// Event delegates to inform other objects when the player has entered or left the trigger zone
	public delegate void ConsoleEvent(GameObject console);
	public static event ConsoleEvent OnTriggerEntered;
	public static event ConsoleEvent OnTriggerLeft;

	void Start () {
		Init ();
	}

	void Init() {
		audio = gameObject.GetComponent<AudioSource> ();
		door = triggerDoor.GetComponent<Door> ();
	}

	// Trigger functions when player reached the trigger zone
	void OnTriggerEnter(Collider col) {
		// Always check, if the collided object is the player
		if (col.gameObject.name == "Player") {
			// Play a sound
			audio.Play();
			// Trigger an event
			OnTriggerEntered (triggerDoor);
		}
	}

	void OnTriggerExit(Collider col) {
		if (col.gameObject.name == "Player") {
			OnTriggerLeft (triggerDoor);
		}
	}

	void OnTriggerStay(Collider col) {
		if (col.gameObject.name == "Player") {
			if(Input.GetKey(KeyCode.Space)) {
				door.isOpen = true;
			}
		}
	}
}
