﻿using UnityEngine;
using System.Collections;

// This class does an simple rotation animation for the bomb objects
public class BombAnimation : MonoBehaviour {
	public float rotationSpeed = 10.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.Rotate (new Vector3 (2, 1, -1)  * Time.deltaTime * rotationSpeed);
	}
}
