﻿using UnityEngine;
using System.Collections;

public class TriggerFinish : MonoBehaviour {
	// Event system, if the player has reached the final target
	public delegate void TriggerEvent();
	public static event TriggerEvent OnTargetReached;

	void OnTriggerEnter(Collider col) {
		if (col.gameObject.name == "Player") {
			OnTargetReached ();
		}
	}
}
