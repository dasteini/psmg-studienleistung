﻿using UnityEngine;
using System.Collections;

// Simple pulse animation for finish target object
public class FinishAnimation : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float val = Mathf.Abs (Mathf.Sin (Time.realtimeSinceStartup))*0.5f + 1f;
		gameObject.transform.localScale = new Vector3 (val, val, val);
	}
}
