﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	// Debug message
	private const string message = "#GameManager#: ";

	// All bombs and doors are referenced here
	private BombTrigger[] bombs;
	private Door[] doors;

	// Reference to Player and UI Manager
	private PlayerManager mPlayerManager;
	private UiManager mUiManager;

	void Start() {
		Init ();
	}

	void Init() {
		// Set events from PlayerManager and the TriggerFinish Object
		PlayerManager.OnHealthChanged += HealthChanged;
		TriggerFinish.OnTargetReached += GameFinished;

		// Find main player
		mPlayerManager = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerManager>();
		mPlayerManager.SetDefaultHealthPoints ();

		// Find UI Manager
		mUiManager = GameObject.Find("Canvas").GetComponent<UiManager>();

		// Enable all bombs
		FindGameObjects();
		ActivateAllBombs ();

	}

	// Enables bombs (on game start or restart)
	void ActivateAllBombs() {
		foreach(BombTrigger bomb in bombs) {
			bomb.EnableBomb (true);
		}
	}

	// Resets all doors to closed state
	void CloseAllDoors() {
		foreach (Door door in doors) {
			door.isOpen = false;
		}
	}

	// Find doors and bombs tagged by "Bomb" and "Door"
	void FindGameObjects() {
		GameObject[] objs = GameObject.FindGameObjectsWithTag("Bomb");
		bombs = new BombTrigger [objs.Length];
		for (int i = 0; i < objs.Length; i++) {
			bombs [i] = objs [i].GetComponent<BombTrigger> ();
		}
		Debug.Log (message + "Found " + bombs.Length + " bombs!");

		objs = GameObject.FindGameObjectsWithTag ("Door");
		doors = new Door [objs.Length];
		for (int i = 0; i < objs.Length; i++) {
			doors [i] = objs [i].GetComponent<Door> ();
		}

		Debug.Log (message + "Found " + doors.Length + " doors!");
	}

	// Callback from PlayerManager, called when health points changed
	void HealthChanged(int points) {
		Debug.Log (message + "Player Health Points: " + points);
		if (mPlayerManager.healthPoint <= 0) {
			GameLost ();
		}
	}

	// Callback from TriggerFinish, is called when player reaches the target
	void GameFinished() {
		ActivateAllBombs ();
		CloseAllDoors ();
		mPlayerManager.respawnPlayer ();
		mPlayerManager.SetDefaultHealthPoints ();
	}

	// Resetting the game partially when the player has lost
	void GameLost() {
		ActivateAllBombs ();
		CloseAllDoors ();
		mPlayerManager.respawnPlayer ();
		mPlayerManager.SetDefaultHealthPoints ();
	}
}
