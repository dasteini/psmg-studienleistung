﻿using UnityEngine;
using System.Collections;

public class PlayerInputManager : MonoBehaviour {
	private Rigidbody mRigidbody;

	// Values for movement and rotation speed
	public float speedBoost = 10.0f;
	public float rotationBoost = 100.0f;

	// Names in the input manager
	private string mMovementAxis = "Move";
	private string mRotationAxis = "Rotate";


	private float mInputMovement = 0.0f;
	private float mInputRotation;

	public void Init() {
		mRigidbody = GetComponent<Rigidbody> ();
	}

	// Movement and Rotation functions from tank example
	private void Move() {
		mInputMovement = Input.GetAxis (mMovementAxis);
		Vector3 movement = transform.forward * mInputMovement * speedBoost * Time.deltaTime;
		mRigidbody.MovePosition (mRigidbody.position + movement);
	}

	private void Rotate() {
		mInputRotation = Input.GetAxis (mRotationAxis);
		float turn = mInputRotation * rotationBoost * Time.deltaTime;
		Quaternion turnRotation = Quaternion.Euler (0f, turn, 0f);
		mRigidbody.MoveRotation (mRigidbody.rotation * turnRotation);
	}

	// Set Player to specified position
	public void setPosition(Transform spawnPosition) {
		mRigidbody.position = spawnPosition.position;
	}


	void FixedUpdate () {
		Move ();
		Rotate ();
	}
}
