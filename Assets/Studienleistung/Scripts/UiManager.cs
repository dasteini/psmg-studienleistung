﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UiManager : MonoBehaviour {
	// Storing all text view references
	private Text pointView;
	private Text timeView;
	private Text messageScreenTimeView;

	// Stores all needed gameObject references
	private GameObject doorButton;
	private GameObject messageScreen;
	private GameObject activeDoor;

	// Health point string
	private const string healthPoints = "Health Points: ";
	private float time, gameStartTime;

	// gameWon variable, to count time up when showing wonScreen for 5 seconds
	bool gameWon = false;

	// Use this for initialization
	void Start () {
		Init ();
	}

	public void Init() {
		// Get all references
		messageScreen = GameObject.Find ("Canvas/MessageScreen");
		pointView = GameObject.Find ("Canvas/Health").GetComponent<Text> ();
		timeView = GameObject.Find ("Canvas/Time").GetComponent<Text> ();
		messageScreenTimeView = GameObject.Find ("Canvas/MessageScreen/Time").GetComponent<Text> ();
		doorButton = GameObject.Find ("Canvas/Button");
		doorButton.SetActive (false);
		messageScreen.SetActive (false);

		// Recieve event messages from other objects
		PlayerManager.OnHealthChanged += UpdatePoints;
		TriggerConsole.OnTriggerEntered += ShowDoorButton;
		TriggerConsole.OnTriggerLeft += HideDoorButton;
		TriggerFinish.OnTargetReached += GameFinished;

		// Set start time
		gameStartTime = Time.time;

	}

	// Show door button when in trigger zone
	void ShowDoorButton(GameObject door) {
		// Show open door button only if the isnt already open
		if (door.GetComponent<Door> ().isOpen == false) {
			doorButton.SetActive (true);
			// Store current triggerDoor in UiManager
			activeDoor = door;
		}
	}

	void HideDoorButton(GameObject door) {
		// When player leaves triggerZone, last active door is deleted
		activeDoor = null;
		doorButton.SetActive (false);
	}

	// this function sets points
	void UpdatePoints(int points) {
		pointView.text = healthPoints + points;
	}

	public void OnDoorButtonClicked() {
		// If player is currently in a triggerZone, open the door
		if (activeDoor != null) {
			activeDoor.GetComponent<Door> ().isOpen = true;
		}
	}

	// Callback when Game is finished
	void GameFinished() {
		messageScreen.SetActive (true);
		time = Time.time - gameStartTime;
		messageScreenTimeView.text = "Took " + (int)time + " to win!";
		gameWon = true;
	}

	// Reset wonScreen after 5 seconds waiting
	void ResetMessageScreen() {
		messageScreen.SetActive (false);
		gameStartTime = Time.time;

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		// Only show seconds
		timeView.text = (int)(Time.time - gameStartTime) + " Seconds";

		// Let the wonScreen stay there for at least 5 seconds
		if (gameWon && (time + 5) < Time.time) {
			gameWon = false;
			ResetMessageScreen ();
		}
	}


}
