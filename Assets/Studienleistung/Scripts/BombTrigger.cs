﻿using UnityEngine;
using System.Collections;

public class BombTrigger : MonoBehaviour {
	// Standard value which is affecting players health
	public int bombValue = 40;
	public delegate void TriggerEvent();
	public static event TriggerEvent OnBombTriggered;

	private BoxCollider mBoxCollider;
	private MeshRenderer mRenderer;
	private AudioSource mAudio;

	// Use this for initialization
	void Start () {
		mBoxCollider = gameObject.GetComponent<BoxCollider> ();
		mRenderer = gameObject.GetComponent<MeshRenderer> ();
		mAudio = gameObject.GetComponent<AudioSource> ();
	}

	void OnTriggerEnter(Collider col) {
		if (col.gameObject.name == "Player") {
			
			// Deactivate bomb trigger
			EnableBomb(false);

			// Decrease player health points
			col.gameObject.GetComponent<PlayerManager> ().decreaseHealthPoints (bombValue);

			// Play explosion sound
			mAudio.Play();


		}
	}

	// Enables or disables (renderer and collider) bomb by passing true or false
	public void EnableBomb(bool activated) {
		mBoxCollider.enabled = activated;
		mRenderer.enabled = activated;
	}

}
